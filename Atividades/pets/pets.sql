-- cliente(cpf,nome, email, ntel)
CREATE TABLE IF NOT EXISTS cliente (
  cpf VARCHAR(11) NOT NULL,
  nome VARCHAR(100) NOT NULL,
  email VARCHAR(100) NOT NULL,
  telefone VARCHAR(20) NOT NULL,
  constraint pk_cliente primary key (cpf)
);

INSERT INTO clientes (cpf, nome, email, telefone) values
  (11111, 'Cruela de Vil', 'cruela@dalmata.com', '11 99932323'),
  (44444, 'Roger Dearly', 'roger@dalmata.com', '11 98761111'),
  (55555, 'Lee Duncan', 'lee@rinty', '21 34512222'),
  (66666, 'Anita Campbell', 'anita@dalmata.com', '11 87679988');

-- raca(cod, descr)
CREATE TABLE IF NOT EXISTS raca(
  cod integer not null,
  descr VARCHAR(100) NOT NULL,
  constraint pk_raca primary key (cod)
);

INSERT INTO raca (cod, descr) values
  ('1', 'Dalmata'),
  ('2', 'Pastor Alemão'),
  ('3', 'Yorkshire');

-- pet(codpet, nome, draca(raca), MATCH SIMPLEdono(cliente), coment)
CREATE TABLE IF NOT EXISTS pet(
  codpet integer not null,
  nome VARCHAR(100) NOT NULL,
  coment VARCHAR(100),
  draca integer not null,
  dono VARCHAR(11) NOT NULL,
  constraint pk_pet primary key (codpet),
  constraint fk_pet_raca foreign key (draca) references raca(cod),
  constraint fk_dono foreign key (dono) references clientes(cpf)
);


INSERT INTO pet(codpet, nome, coment) values
  (1, 'Pongo', 1, '44444'),
  (2, 'Rin tin tin', 2, '55555', 'Muito dócil'),
  (3, 'Perdy', 1, '66666', 'Alérgica a sabão'),
  (4, 'Benji', 3, '11111');

-- historico(idhist, codpet(pet), data, medida, valor, unid)
CREATE TABLE IF NOT EXISTS histr(
  idhist integer not null,
  datahist VARCHAR(100) NOT NULL,
  medida VARCHAR(100) NOT NULL,
  valor VARCHAR(100) NOT NULL,
  unid VARCHAR(100) NOT NULL,
  ccodpet integer not null,
  constraint pk_histr primary key (idhist),
  constraint fk_histr_pet foreign key (ccodpet) references pet(codpet)
);

INSERT INTO histr (idhist, datahist, medida, valor, unid, ccodpet) values
  (1, 1, '10/05/2018', 'peso', '10.5', 'KG'),
  (2, 1, '10/05/2018', 'altura', '98', 'CM'),
  (3, 1, '10/05/2018', 'comprimento', '1.23', 'M'),
  (4, 2, '05/06/1975', 'peso', '14.56', 'KG'),
  (5, 2, '05/06/1975', 'altura', '1.2', 'M'),
  (6, 3, '12/08/2018', 'peso', '9.8', 'KG'),
  (7, 1, '14/08/2018', 'peso', '11.2', 'KG'),
  (8, 5, '11/11/2011', 'altura', '89', 'CM');