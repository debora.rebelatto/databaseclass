-- CREATE table products with id as a pk, name, amount and price
CREATE TABLE products (
    id INTEGER PRIMARY KEY AUTOINCREMENT,0
    name TEXT,
    amount INTEGER,
    price INTEGER
);

insert into products (name, amount, price) values
  ('TWO DOORS', '160', '800'),
  ('Dining table', '1000', '520'),
  ('Towel', '500', '100');

