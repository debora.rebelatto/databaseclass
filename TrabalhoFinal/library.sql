-- create author table
CREATE TABLE IF NOT EXISTS author (
  id_author int(11) NOT NULL,
  name varchar(255) NOT NULL,
  constraint pk_author primary key (id_author)
);

-- create book table
CREATE TABLE IF NOT EXISTS book (
  id_book INTEGER NOT NULL,
  title varchar(255) NOT NULL,
  isbn varchar(255) NOT NULL,
  publicacao VARCHAR(255) NOT NULL,
  pageNumber INTEGER NOT NULL,
  genero varchar(255) NOT NULL,
  id_author INTEGER NOT NULL,
  constraint pk_book primary key (id_book),
  constraint fk_author foreign key (id_author) references author(id_author)
);

-- create person table
CREATE TABLE IF NOT EXISTS person (
  id int(11) NOT NULL,
  name varchar(255) NOT NULL,
  cpf VARCHAR(255) NOT NULL
);

create table renting(
  id_person integer,
  id_book integer,
  day date,
  constraint renting_pk primary key (id_person, id_book, day),
  constraint reserve_person_fk foreign key (id_person) references sailors(id_person),
  constraint book_fk foreign key (id_book) references book(id_book)
);

-- insert authors
insert into author (id_author, name) values
  (1, 'Andrew Piper'),
  (2, 'Neil Gaiman'),
  (3, 'Mary Shelley'),
  (4, 'Oscar Wilde'),
  (5, 'Bram Stoker'),
  (6, 'John Milton'),
  (7, 'H. P. Lovecraft'),
  (8, 'MAYA ANGELOU E JEAN-MICHEL BASQUIAT'),
  (9, 'KATHERINE DUNN'),
  (10, 'Edgar Allan Poe');

-- insert books
insert into book (id_book, title, isbn, publicacao, pageNumber, genero, id_author) values
  (1, 'Frankenstein',                           '9788594540188', '2017', 304, 'Terror',   3),
  (2, 'O Retrato de Dorian Gray',               '9786555980004', '2021', 320, 'Ficção',   4),
  (3, 'PARAÍSO PERDIDO',                        '9788594541062', '2021', 320, 'Ficção',   6),
  (4, 'O DEMONOLOGISTA',                        '9788566636406', '2015', 320, 'Ficção',   1),
  (5, 'OS CONDENADOS',                          '9788566636352', '2016', 336, 'Ficção',   1),
  (6, 'A CRIATURA',                             '9788594541055', '2020', 304, 'Ficção',   1),
  (7, 'SERES MÁGICOS & HISTÓRIAS SOMBRIAS',     '9788594540973', '2019', 448, 'Drama',    2),
  (8, 'DRÁCULA - DARK EDITION',                 '9788566636239', '2018', 580, 'Terror',   5),
  (9, 'Coraline',                               '8551006754',    '2020', 224, 'Conto',    2),
  (10, 'O Oceano no Fim do Caminho',            '8580573696',    '2013', 208, 'Romance',  2),
  (11, 'H.P. LOVECRAFT - MISKATONIC EDITION',   '9788594540782', '2017', 400, 'Terror',   7),
  (12, 'H.P. LOVECRAFT - COSMIC EDITION',       '9788594540799', '2018', 400, 'Terror',   7),
  (13, 'A VIDA NÃO ME ASSUSTA',                 '9788594541154', '2017', 48,  'Poesia',   8),
  (14, 'GEEK LOVE',                             '9788594540447', '2018', 368, 'Fantasia', 9),
  (15, 'EDGAR ALLAN POE: MEDO CLÁSSICO VOL. 1', '9788594540249', '2017', 368, 'Terror',   10),
  (16, 'EDGAR ALLAN POE: MEDO CLÁSSICO VOL. 2', '9788594540250', '2018', 368, 'Terror',   10);

INSERT INTO person (id, name, cpf) VALUES
  (1, 'João', '12345678901'),
  (2, 'Maria', '12345678902'),
  (3, 'Pedro', '12345678903'),
  (4, 'Ana', '12345678904'),
  (5, 'José', '12345678905'),
  (6, 'Paulo', '12345678906');


-- insert renting
INSERT INTO renting (id_person, id_book, day) VALUES
  (1, 1, '2021-01-01'),
  (2, 1, '2020-02-07'),
  (3, 1, '2021-03-06'),
  (4, 4, '2020-01-05'),
  (5, 5, '2020-05-01'),
  (6, 6, '2021-06-04'),
  (1, 7, '2020-07-01'),
  (1, 8, '2019-02-03'),
  (6, 9, '2020-01-01'),
  (4, 10, '2020-10-20'),
  (6, 11, '2020-11-13'),
  (3, 12, '2020-01-14'),
  (2, 13, '2020-01-23'),
  (2, 14, '2020-01-5'),
  (2, 2, '2020-01-7'),
  (2, 3, '2020-01-01'),
  (2, 5, '2020-01-8');

-- book name and author name
SELECT B.title, A.name
  FROM book B
  NATURAL JOIN author A;

-- Book
SELECT B.title, A.name
  FROM book B
  NATURAL JOIN author A WHERE A.id_author = 7;

-- returns all books that author name starts with the letter N
SELECT B.title FROM book B NATURAL JOIN author A WHERE A.name LIKE 'N%';

-- Return all books from 2021
SELECT B.title FROM book B WHERE B.publicacao = '2021';

-- return all books from 2019 and its isbn
SELECT B.title, B.isbn FROM book B WHERE B.publicacao = '2019';


-- select books with the name 'Frankenstein' that where rented and the name of who rented
SELECT B.title, P.name
  FROM book B
  NATURAL JOIN renting R
  NATURAL JOIN person P
  WHERE B.title = 'Frankenstein' AND R.id_book = B.id_book;


-- select name and cpf of everyone who rented book with id = 5
SELECT P.name, P.cpf
  FROM person P
  NATURAL JOIN renting R
  WHERE R.id_book = 5;


-- select all books rented by João that are Terror
SELECT R.day, B.title
  FROM book B
  NATURAL JOIN renting R
  NATURAL JOIN person P
  WHERE P.name = 'João' AND B.genero = 'Terror';

-- select all books that were rented by Pedro and were published in 2021
SELECT R.day, B.title
  FROM book B
  NATURAL JOIN renting R
  NATURAL JOIN person P
  WHERE P.name = 'Pedro' AND B.publicacao = '2021';

-- select all books with more than 300 pages
SELECT B.pageNumber, B.title
  FROM book B
  WHERE B.pageNumber > 340;